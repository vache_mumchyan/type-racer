package com.example.typeracer

import android.app.Application
import com.example.typeracer.di.networkModule
import com.example.typeracer.di.repositoryModule
import com.example.typeracer.di.roomModule
import com.example.typeracer.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TypeRacerApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@TypeRacerApp)
            modules(listOf(viewModelModule, networkModule, repositoryModule, roomModule))
        }

    }
}