package com.example.typeracer.utils

import android.widget.EditText
import androidx.databinding.BindingAdapter

class BindingsUtils {

    companion object {
        @JvmStatic
        @BindingAdapter("removeData")
        fun loadImage(view: EditText, text: String?) {
            if (!text.isNullOrEmpty()) {
                view.setText("")
            }
        }
    }
}