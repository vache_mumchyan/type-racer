package com.example.typeracer.utils

class Utils {

    companion object{
        fun getRandomString(length: Int) : String {
            val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz"
            return (1..length)
                .map { allowedChars.random() }
                .joinToString("")
        }
    }
}