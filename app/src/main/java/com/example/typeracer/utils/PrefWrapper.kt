package com.example.typeracer.utils

import android.content.Context

class PrefWrapper(private val context: Context){

    companion object {
        const val PREF_FILE_KEY = "pref_file_key"
    }

    private val pref = context.applicationContext.getSharedPreferences(PREF_FILE_KEY, Context.MODE_PRIVATE)

    fun set(key: String, value: String?) {
        pref.edit()
            .putString(key, value)
            .apply()
    }

    fun get(key: String, defValue: String?): String? = pref.getString(key, defValue)

    fun set(key: String, value: Int) {
        pref.edit()
            .putInt(key, value)
            .apply()
    }

    fun get(key: String, defValue: Int): Int = pref.getInt(key, defValue)

    fun set(key: String, value: Float) {
        pref.edit()
            .putFloat(key, value)
            .apply()
    }

    fun get(key: String, defValue: Float): Float = pref.getFloat(key, defValue)

    fun set(key: String, value: Boolean) {
        pref.edit()
            .putBoolean(key, value)
            .apply()
    }

    fun get(key: String, defValue: Boolean): Boolean = pref.getBoolean(key, defValue)
}