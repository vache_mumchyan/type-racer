package com.example.typeracer.data.repository.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query

interface TextApi {

    @GET("api/")
    fun getText(@Query("type") type: String): Single<List<String>>

}