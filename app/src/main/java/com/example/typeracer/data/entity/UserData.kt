package com.example.typeracer.data.entity

data class UserData (val nickname: String, val history: List<HistoryItem>)