package com.example.typeracer.data.repository.local

import com.example.typeracer.data.entity.HistoryItem

class DBDataSource (private val dao: HistoryDao) {

    fun getAll() = dao.getAll()

    fun insertAll(history: List<HistoryItem>) = dao.insertAll(history)

    fun removeAll() = dao.removeAll()

    fun insert(historyItem: HistoryItem) = dao.insert(historyItem)

}