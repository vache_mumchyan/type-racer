package com.example.typeracer.data.entity

data class GameResult(val speed: Int, val wpm: Int, val percentage: Int)