package com.example.typeracer.data.repository.local

import com.example.typeracer.utils.PrefWrapper

class PrefDataSource(val prefs: PrefWrapper) {

    companion object{
        const val ID_KEY = "id_key"
        const val ID_NICKNAME = "nickname_key"
    }

    var id: String? = null
    set(value) {
        field = value
        return prefs.set(ID_KEY, value)
    }
    get() {
        return prefs.get(ID_KEY, null)
    }

    var nickname: String = ""
        set(value) {
            field = value
            return prefs.set(ID_NICKNAME, value)
        }
        get() {
            return prefs.get(ID_NICKNAME, "")!!
        }

}
