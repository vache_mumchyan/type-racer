package com.example.typeracer.data.repository.local

import com.example.typeracer.data.entity.HistoryItem

class LocalRepository(
    private val dbDataSource: DBDataSource,
    private val prefDataSource: PrefDataSource
) {

    fun getAll() = dbDataSource.getAll()

    fun insertAll(history: List<HistoryItem>) = dbDataSource.insertAll(history)

    fun removeHistory() = dbDataSource.removeAll()

    fun insert(historyItem: HistoryItem) = dbDataSource.insert(historyItem)

    fun setId(id: String?) {
        prefDataSource.id = id
    }

    fun getId() = prefDataSource.id

    fun setNickName(nickname: String) {
        prefDataSource.nickname = nickname
    }

    fun getNickName() = prefDataSource.nickname

    fun clearAllData() {
        removeHistory()
        prefDataSource.nickname = ""
        prefDataSource.id = null
    }

}