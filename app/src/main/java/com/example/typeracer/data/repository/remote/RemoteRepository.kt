package com.example.typeracer.data.repository.remote

import com.example.typeracer.data.entity.HistoryItem
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single

class RemoteRepository(private val userApi: UserApi, private val textApi: TextApi) {

    fun getUserData(id: String) = userApi.getData(id)

    fun getText(type: String) = textApi.getText(type)

    fun registration(nickname: String): Single<String> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("nickname", nickname)
        return userApi.registration(jsonObject)
            .map { it.uri.split("/").last() }
    }

    fun updateUserData(id: String, nickname: String, history: List<HistoryItem>) : Completable{
        val jsonObject = JsonObject()
        jsonObject.addProperty("nickname", nickname)
        val historyJson = Gson().toJsonTree(history)
        jsonObject.add("history", historyJson)
        return userApi.updateHistory(id, jsonObject)
    }

}