package com.example.typeracer.data.repository.remote

import com.example.typeracer.data.entity.RegistrationResponse
import com.example.typeracer.data.entity.UserData
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface UserApi {

    @GET("bins/{id}")
    fun getData(@Path("id") id: String) : Single<UserData>

    @POST("bins")
    fun registration(@Body jsonObject: JsonObject): Single<RegistrationResponse>

    @PUT("bins/{id}")
    fun updateHistory(@Path("id") id: String, @Body jsonObject: JsonObject): Completable

}