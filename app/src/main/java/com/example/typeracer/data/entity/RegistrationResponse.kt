package com.example.typeracer.data.entity

data class RegistrationResponse(val uri: String)