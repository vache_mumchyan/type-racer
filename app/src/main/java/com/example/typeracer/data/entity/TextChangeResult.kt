package com.example.typeracer.data.entity

data class TextChangeResult(
    val correctEntries: Int,
    val incorrectEntries: Int,
    val correctWords: Int
)