package com.example.typeracer.data.repository.local

import androidx.room.*
import com.example.typeracer.data.entity.HistoryItem
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.PUT

@Dao
interface HistoryDao {

    @Query("SELECT * FROM history")
    fun getAll(): Single<List<HistoryItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(history: List<HistoryItem>): Completable

    @Query("DELETE  FROM history")
    fun removeAll(): Completable

    @Insert
    fun insert(history: HistoryItem): Completable

}