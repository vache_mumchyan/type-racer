package com.example.typeracer.data.repository.local

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Example(val name:String, @Transient val age: Int) : Parcelable