package com.example.typeracer.data.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.typeracer.data.entity.HistoryItem

@Database(
    entities = [HistoryItem::class],
    version = DemoDatabase.VERSION
)
abstract class DemoDatabase : RoomDatabase() {
    abstract fun historyDao(): HistoryDao

    companion object {
        const val VERSION = 1
    }
}