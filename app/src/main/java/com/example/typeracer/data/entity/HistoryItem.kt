package com.example.typeracer.data.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "history")
@Parcelize
data class HistoryItem(
    @PrimaryKey(autoGenerate = true)
    val date: Long,
    val accuracy: Float,
    val correctWords: Int,
    val correctEntries: Int,
    val incorrectEntries: Int
) : Parcelable