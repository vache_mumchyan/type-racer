package com.example.typeracer.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.typeracer.R
import com.example.typeracer.data.repository.local.LocalRepository
import com.example.typeracer.ui.history.HistoryFragment
import com.example.typeracer.ui.signin.SignInFragment
import com.google.gson.Gson
import com.google.gson.JsonElement
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {

    private val localRepository: LocalRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        localRepository.getId()?.let {
             addFragment(HistoryFragment.newInstance())
        } ?: addFragment(SignInFragment.newInstance())
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, fragment)
            .commit()
    }


}
