package com.example.typeracer.ui.game

import android.os.CountDownTimer
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.typeracer.data.entity.HistoryItem
import com.example.typeracer.data.entity.TextChangeResult
import com.example.typeracer.data.repository.remote.RemoteRepository
import com.example.typeracer.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class GameViewModel(private val remoteRepository: RemoteRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val text = ObservableField<String>()
    val inProgress = ObservableField<Boolean>()
    val inBeforeTimer = ObservableField<Boolean>()
    val countDown = ObservableField<Int>()
    val second = ObservableField<Int>()
    val speed = ObservableField<Int>()
    val accuracy = ObservableField<Float>()
    val openDialog = MutableLiveData<HistoryItem>()
    val keyBoardIsOpen = MutableLiveData<Boolean>()

    val gameDuration = 1

    var currCorrectEntries = 0
    var currIncorrectEntries = 0

    var beforeStartGameTimer = 3


    val timerCountDown =  object : CountDownTimer((gameDuration * 8000).toLong(), 1000) {
        override fun onTick(millisUntilFinished: Long) {
            second.set((millisUntilFinished / 1000).toInt())
        }

        override fun onFinish() {
            second.set(0)
            val currAccuracy = accuracy.get() ?: 0f
            val currSpeed = speed.get() ?: 0
            openDialog.value = HistoryItem(
                System.currentTimeMillis(), currAccuracy,
                currSpeed, currCorrectEntries, currIncorrectEntries
            )
        }
    }

    val onTextChangeListener = object : OnTextChangeListener {
        override fun onTextChange(textChangeResult: TextChangeResult) {
            textChangeResult.run {
                val currCurrency = String.format(
                    "%.2f",
                    calculateCurrency(correctEntries, incorrectEntries)
                ).toFloat()

                accuracy.set(currCurrency)
                speed.set(correctWords / gameDuration)

                currCorrectEntries = correctEntries
                currIncorrectEntries = incorrectEntries
            }
        }
    }

    fun init() {
        inProgress.set(true)
        remoteRepository.getText(Utils.getRandomString(4))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                var allText = ""
                it.forEach { s ->
                    allText += s
                }
                text.set(allText)
                inProgress.set(false)
                beforeGameStartTimer()
            }, {
                inProgress.set(false)
                //error handling
            }).let { compositeDisposable.add(it) }
    }

    private fun beforeGameStartTimer() {
        inBeforeTimer.set(true)
        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                countDown.set(beforeStartGameTimer)
                beforeStartGameTimer--
            }

            override fun onFinish() {
                inBeforeTimer.set(false)
                keyBoardIsOpen.value = true
                timerCountDown.start()
            }
        }.start()
    }

    private fun calculateCurrency(correctEntries: Int, incorrectEntries: Int): Float {
        return if (incorrectEntries == 0) {
            100f
        } else {
            (correctEntries / (correctEntries + incorrectEntries).toFloat()) * 100
        }

    }

    override fun onCleared() {
        super.onCleared()
        timerCountDown.cancel()
    }
}

interface OnTextChangeListener {
    fun onTextChange(textChangeResult: TextChangeResult)
}