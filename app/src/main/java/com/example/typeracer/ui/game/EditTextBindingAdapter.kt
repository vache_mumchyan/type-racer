package com.example.typeracer.ui.game

import android.graphics.Color
import android.text.Editable
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.widget.EditText
import androidx.annotation.ColorInt
import androidx.databinding.BindingAdapter
import com.example.typeracer.data.entity.TextChangeResult
import com.example.typeracer.ui.game.OnTextChangeListener

object EditTextBindingAdapter {

    @JvmStatic
    @BindingAdapter("setText", "onTextChanged", requireAll = true)
    fun onTextChanged(view: EditText, t: String?, onTextChangeListener: OnTextChangeListener) {
        t?.let { text ->
            var correctEntries = 0
            var incorrectEntries = 0
            var correctWords = 0
            var correctWordsCounter = 1

            val textBuilder = SpannableStringBuilder(text)
            view.text = textBuilder

            var isKeyTyped = false
            view.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    isKeyTyped = false
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (!isKeyTyped) {
                        isKeyTyped = true

                        val index = view.selectionStart - 1
                        val currChar = s?.let {
                            it[index]
                        }

                        if (textBuilder[index] == currChar) {
                            correctWordsCounter = correctWordsCounter and 1
                            correctEntries++
                            putColor(textBuilder, index, Color.GREEN)
                        } else {
                            correctWordsCounter = correctWordsCounter and 0
                            incorrectEntries++
                            putColor(textBuilder, index, Color.RED)
                        }

                        if (textBuilder[index] == ' ') {
                            correctWords += correctWordsCounter
                            correctWordsCounter = 1
                        }

                        view.text = textBuilder
                        if (textBuilder.length > index + 1) {
                            view.setSelection(index + 1)
                        }

                        onTextChangeListener.onTextChange(
                            TextChangeResult(
                                correctEntries,
                                incorrectEntries,
                                correctWords
                            )
                        )
                    }
                }
            })
        }

    }

    private fun putColor(textBuilder: SpannableStringBuilder, index: Int, @ColorInt color: Int) {
        if (textBuilder[index] == ' ') {
            textBuilder.setSpan(
                BackgroundColorSpan(color),
                index,
                index + 1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        } else {
            textBuilder.setSpan(
                ForegroundColorSpan(color),
                index,
                index + 1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}