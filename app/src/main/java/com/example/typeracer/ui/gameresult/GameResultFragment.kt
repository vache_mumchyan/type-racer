package com.example.typeracer.ui.gameresult

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.typeracer.R
import com.example.typeracer.data.entity.HistoryItem
import com.example.typeracer.databinding.FragmentGameResultBinding
import com.example.typeracer.ui.game.GameFragment
import com.example.typeracer.ui.history.HistoryFragment
import kotlinx.android.synthetic.main.fragment_game_result.*
import org.koin.android.viewmodel.ext.android.viewModel

private const val HISTORY_ITEM = "history_item"

class GameResultFragment : Fragment() {

    private var data: HistoryItem? = null
    private val viewModel: GameResultViewModel by viewModel()

    companion object {
        fun newInstance(data: HistoryItem) =
            GameResultFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(HISTORY_ITEM, data)
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGameResultBinding =
            FragmentGameResultBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            it.getParcelable<HistoryItem>(HISTORY_ITEM)
                ?.let { historyItem -> viewModel.init(historyItem) }
        }

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (activity != null) {
                    activity!!.supportFragmentManager.beginTransaction().replace(
                        R.id.container,
                        HistoryFragment.newInstance()
                    ).commit()
                }
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        viewModel.playAgain.observe(this,
            Observer {
            activity!!.supportFragmentManager.beginTransaction().replace(
                R.id.container,
                GameFragment.newInstance()
            ).commit()
        })

    }

}
