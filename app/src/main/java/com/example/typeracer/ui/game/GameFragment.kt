package com.example.typeracer.ui.game

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.typeracer.R
import com.example.typeracer.data.entity.HistoryItem
import com.example.typeracer.databinding.FragmentGameBinding
import com.example.typeracer.ui.gameresult.GameResultFragment
import com.example.typeracer.ui.history.HistoryFragment
import kotlinx.android.synthetic.main.fragment_game.*
import org.koin.android.viewmodel.ext.android.viewModel


class GameFragment : Fragment() {

    private val viewModel: GameViewModel by viewModel()

    companion object {
        fun newInstance() = GameFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity!!.supportFragmentManager.beginTransaction().replace(
                    R.id.container,
                    HistoryFragment.newInstance()
                ).commit()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGameBinding =
            FragmentGameBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.init()

        viewModel.keyBoardIsOpen.observe(this, Observer {
            showSoftKeyboard(textData)
        })

        viewModel.openDialog.observe(this, Observer {

            activity!!.supportFragmentManager.beginTransaction().replace(
                R.id.container,
                GameResultFragment.newInstance(it)
            ).commit()
        })
    }

    private fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}
