package com.example.typeracer.ui.history

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.typeracer.data.entity.HistoryItem
import com.example.typeracer.data.entity.UserData
import com.example.typeracer.data.repository.local.LocalRepository
import com.example.typeracer.data.repository.remote.RemoteRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HistoryViewModel(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val data = MutableLiveData<List<HistoryItem>>()
    val nickName = ObservableField<String>()
    val inProgress = ObservableField<Boolean>()
    val id = MutableLiveData<String>()
    val nextGameFragment = MutableLiveData<Boolean>()
    val logOut = MutableLiveData<Boolean>()


    fun init() {
       // nickName.value = localRepository.getNickName()
        inProgress.set(true)
        //11vl6o
        //k788g
        id.value = localRepository.getId()
        localRepository.getId()?.let { id ->
            remoteRepository.getUserData(id)
                .flatMap { userData ->
                    return@flatMap localRepository.removeHistory()
                        .andThen(localRepository.insertAll(userData.history))
                        .andThen(Single.just(userData))
                }
                .onErrorResumeNext {
                    inProgress.set(false)
                    return@onErrorResumeNext localRepository.getAll()
                        .map {
                          UserData(localRepository.getNickName(), it)
                        }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    inProgress.set(false)
                    it.run {
                        nickName.set(nickname)
                        data.value = history
                    }
                }, {
                    inProgress.set(false)
                }).let {
                    compositeDisposable.add(it)
                }
        }
    }

    fun startClick() {
        nextGameFragment.value = true
    }

    fun logOut() {
        localRepository.clearAllData()
        logOut.value = true
    }
}