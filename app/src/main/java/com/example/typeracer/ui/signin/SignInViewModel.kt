package com.example.typeracer.ui.signin

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.typeracer.data.repository.local.LocalRepository
import com.example.typeracer.data.repository.remote.RemoteRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SignInViewModel(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository
) : ViewModel() {

    val id = ObservableField<String>()
    val nickname = ObservableField<String>()
    val nextFragment = MutableLiveData<Boolean>()
    val checkInput = MutableLiveData<String>()
    val emptyFields = MutableLiveData<Boolean>()
    val userNotFound = MutableLiveData<String>()
    val inProgress = ObservableField<Boolean>()

    private val compositeDisposable = CompositeDisposable()

    fun continueClick() {
            inProgress.set(true)
            if (!id.get().isNullOrEmpty()) {
                    remoteRepository.getUserData(id.get()!!)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            nextFragment.value = true
                            localRepository.setId(id.get())
                            inProgress.set(false)
                        }, {
                            print(it.message)
                            inProgress.set(false)
                            userNotFound.value = "User not Found"
                        }).let {compositeDisposable.add(it) }
            }else if (!nickname.get().isNullOrEmpty()){
                remoteRepository.registration(nickname.get()!!)
                    .flatMapCompletable {id ->
                        localRepository.run {
                            setId(id)
                            setNickName(nickname.get()!!)
                        }
                        return@flatMapCompletable  localRepository.removeHistory()
                    }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        nextFragment.value = true
                        inProgress.set(false)
                    },{
                        userNotFound.value = "Server doesn't answer"
                        inProgress.set(false)
                    })

            }else{
                inProgress.set(false)
                emptyFields.value = true
            }
    }
}