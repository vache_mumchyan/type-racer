package com.example.typeracer.ui.gameresult

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.typeracer.data.entity.HistoryItem
import com.example.typeracer.data.repository.local.LocalRepository
import com.example.typeracer.data.repository.remote.RemoteRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GameResultViewModel(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository
) : ViewModel() {

    val historyItem = ObservableField<HistoryItem>()
    val playAgain = MutableLiveData<Boolean>()
    private val compositeDisposable = CompositeDisposable()

    fun init(item: HistoryItem) {
        historyItem.set(item)

        localRepository.insert(item)
            .andThen(localRepository.getAll())
            .flatMapCompletable{
                val id = localRepository.getId() ?: ""
                val nickname = localRepository.getNickName()
                return@flatMapCompletable remoteRepository.updateUserData(id, nickname, it)
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
            }.let { compositeDisposable.add(it) }

    }


    fun playAgain() {
        playAgain.value = true
        compositeDisposable.clear()
    }
}