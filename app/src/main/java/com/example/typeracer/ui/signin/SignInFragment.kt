package com.example.typeracer.ui.signin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.typeracer.R
import com.example.typeracer.databinding.FragmentSignInBinding
import com.example.typeracer.ui.history.HistoryFragment
import org.koin.android.viewmodel.ext.android.viewModel

class SignInFragment : Fragment() {

    private val viewModel: SignInViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentSignInBinding =
            FragmentSignInBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.nextFragment.observe(this, Observer {

            activity!!.supportFragmentManager.beginTransaction().replace(
                R.id.container,
                HistoryFragment.newInstance()
            ).commit()
        })

        viewModel.checkInput.observe(this, Observer {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()

        })

        viewModel.userNotFound.observe(this, Observer {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.emptyFields.observe(this, Observer {
            Toast.makeText(context, "Please enter ID or Nickname ", Toast.LENGTH_SHORT).show()
        })
    }

    companion object {
        fun newInstance() = SignInFragment()
    }
}
