package com.example.typeracer.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.typeracer.R
import com.example.typeracer.databinding.FragmentHistoryBinding
import com.example.typeracer.ui.game.GameFragment
import com.example.typeracer.ui.signin.SignInFragment
import kotlinx.android.synthetic.main.fragment_history.*
import org.koin.android.viewmodel.ext.android.viewModel


class HistoryFragment : Fragment() {

    private val viewModel: HistoryViewModel by viewModel()
    private lateinit var historyAdapter: HistoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentHistoryBinding =
            FragmentHistoryBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.init()
        history_recycler.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        historyAdapter = HistoryAdapter()
        history_recycler.adapter = historyAdapter

        viewModel.data.observe(this, Observer {
            historyAdapter.listHistory = it
        })

        viewModel.nextGameFragment.observe(this, Observer {
                activity!!.supportFragmentManager.beginTransaction().add(
                    R.id.container,
                    GameFragment.newInstance()
                ).addToBackStack(null).commit()

        })

        viewModel.logOut.observe(this, Observer {
            activity!!.supportFragmentManager.beginTransaction().replace(
                R.id.container,
                SignInFragment.newInstance()
            ).commit()
        })

    }

    companion object {
        fun newInstance() = HistoryFragment()
    }
}
