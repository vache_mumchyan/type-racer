package com.example.typeracer.di

import androidx.room.Room
import com.example.typeracer.data.repository.local.DemoDatabase
import com.example.typeracer.data.repository.local.DBDataSource
import com.example.typeracer.data.repository.local.LocalRepository
import com.example.typeracer.data.repository.local.PrefDataSource
import com.example.typeracer.data.repository.remote.UserApi
import com.example.typeracer.data.repository.remote.TextApi
import com.example.typeracer.data.repository.remote.RemoteRepository
import com.example.typeracer.ui.game.GameViewModel
import com.example.typeracer.ui.gameresult.GameResultViewModel
import com.example.typeracer.ui.history.HistoryViewModel
import com.example.typeracer.ui.signin.SignInViewModel
import com.example.typeracer.utils.PrefWrapper
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {

    viewModel { GameViewModel(get()) }
    viewModel { GameResultViewModel(get(), get()) }
    viewModel { SignInViewModel(get(), get()) }
    viewModel { HistoryViewModel(get(), get()) }

}

val networkModule = module {
    single { provideApi(get(named("userApi"))) }
    single(named("userApi")) { provideRetrofit() }
    single { provideBaconApi(get(named("textApi"))) }
    single(named("textApi")) { provideBaconIspum() }

}

val repositoryModule = module {
    single { RemoteRepository(get(), get()) }
    single { DBDataSource(get()) }
    single { PrefDataSource(get()) }
    single { LocalRepository(get(), get()) }
    single { PrefWrapper(get()) }
}

val roomModule = module {
    single {
        Room.databaseBuilder(get(), DemoDatabase::class.java, "history-db").build()
    }
    single { get<DemoDatabase>().historyDao() }
}

fun provideBaconIspum(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://baconipsum.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun provideBaconApi(retrofit: Retrofit): TextApi =
    retrofit.create(TextApi::class.java)

fun provideRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://api.myjson.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun provideApi(retrofit: Retrofit): UserApi =
    retrofit.create(UserApi::class.java)
